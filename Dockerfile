FROM tomcat:latest
RUN apt-get update
RUN apt-get install default-jdk -y
RUN apt-get install maven git -y
WORKDIR /usr/test
RUN git clone https://github.com/boxfuse/boxfuse-sample-java-war-hello.git /usr/test
RUN mvn package
RUN cp /usr/test/target/hello-1.0.war /usr/local/tomcat/webapps
EXPOSE 8080
CMD ["catalina.sh","run"]
